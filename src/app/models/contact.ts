
export interface iUser {
  id?: number;
  email: string;
  name: string;
  phone: string;
  username: string;
  website: string;
  company: iCompany;
  address: iAddress;
}

export interface iCompany {
  bs: string;
  catchPhrase: string;
  name: string;
}

export interface iAddress {
  city: string;
  geo: any;
  street: string;
  suite: string;
  zipcode: string;
}

export function userDTO(
  id: number,
  email: string,
  name: string,
  phone: string,
  username: string,
  website: string,
  company: iCompany,
  address: iAddress
): iUser {
  return {
    id: id,
    email: email,
    name: name,
    phone: phone,
    username: username,
    website: website,
    company: company,
    address: address,
  };
}
