import { iEdit } from './../contacts/contacts.component';
import { UserService } from './../services/user.service';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.scss'],
})
export class FormUserComponent implements OnInit, OnChanges {
  formGroup!: FormGroup;
  isLinear = false;
  id: string | null = '';
  action: string = 'new';

  @Input() userId!: iEdit;
  @Output() submitAction = new EventEmitter();

  constructor(
    private userService: UserService,
    private actRouted: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {
    this.createForm();
  }
  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes);
    // check when data changes
    if (changes.userId && changes.userId.currentValue) {
      const userId = changes.userId.currentValue.userId;
      this.userService.getById(userId).subscribe((response) => {
        this.formGroup.setValue(response);
        this.action = changes.userId.currentValue.action;
      });
    }
  }

  ngOnInit(): void {
    // this work if not using reusable component
    this.id = this.actRouted.snapshot.paramMap.get('id');
    if (this.id) {
      this.userService.getById(this.id).subscribe((response) => {
        this.formGroup.setValue(response);
      });
    }
  }

  createForm() {
    this.formGroup = new FormGroup({
      id: new FormControl(''),
      name: new FormControl('', { validators: [Validators.required] }),
      username: new FormControl('', { validators: [Validators.required] }),
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
      }),
      phone: new FormControl('', {
        validators: [Validators.required, Validators.pattern('^[0-9]*$')],
      }),
      website: new FormControl('', { validators: [Validators.required] }),
      address: new FormGroup({
        city: new FormControl('', { validators: [Validators.required] }),
        geo: new FormGroup({
          lat: new FormControl('', { validators: [Validators.required] }),
          lng: new FormControl('', { validators: [Validators.required] }),
        }),
        street: new FormControl('', { validators: [Validators.required] }),
        suite: new FormControl('', { validators: [Validators.required] }),
        zipcode: new FormControl('', { validators: [Validators.required] }),
      }),
      company: new FormGroup({
        bs: new FormControl('', { validators: [Validators.required] }),
        catchPhrase: new FormControl('', { validators: [Validators.required] }),
        name: new FormControl('', { validators: [Validators.required] }),
      }),
    });
  }

  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  public get addressGroup(): FormGroup {
    return this.formGroup.get('address') as FormGroup;
  }

  onSubmit() {
    // this.userService.post(this.formGroup.value)
    this.spinner.show();
    this.submitAction.emit({ action: this.action, data: this.formGroup });
  }

  onReset() {
    this.formGroup.reset();
  }
}
