import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { iUser } from '../models/contact';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private httpClient: HttpClient) {}
  private contacts: any = [];
  private $updated = new Subject<any>();

  getUpdatedListener() {
    return this.$updated.asObservable();
  }

  // get(): Observable<any> {
  //   return this.httpClient.get(`${environment.api}/users`).pipe(
  //     map((response) => response),
  //     catchError((err) => err)
  //   );
  // }

  get() {
    this.httpClient
      .get(`${environment.api}/users`)
      .pipe(
        map((response) => response),
        catchError((err) => err)
      )
      .subscribe((resp) => {
        this.contacts = resp;
        this.$updated.next({
          contacts: [...this.contacts],
        });
      });
  }

  getById(params: number | string): Observable<any> {
    return this.httpClient.get(`${environment.api}/users/${params}`).pipe(
      map((response) => response),
      catchError((err) => err)
    );
  }

  put(body: iUser, userId: number | string) {
    this.httpClient
      .put(`${environment.api}/users/${userId}`, body)
      .pipe(
        map((response) => response),
        catchError((err) => err)
      )
      .subscribe((result) => {
        const newState = [...this.contacts];
        const targetIndex = this.contacts.findIndex((f:any)=>f.id===userId);
        newState[targetIndex] = result;
        this.$updated.next({
          contacts: [...newState],
        });
      });
  }

  post(body: iUser) {
    this.httpClient
      .post(`${environment.api}/users`, body)
      .pipe(
        map((response) => response),
        catchError((err) => err)
      )
      .subscribe((result) => {
        this.$updated.next({
          contacts: [...this.contacts, result],
        });
      });
  }

  delete(userId: number | string) {
    this.httpClient
      .delete(`${environment.api}/users/${userId}`)
      .pipe(
        map((response) => response),
        catchError((err) => err)
      )
      .subscribe(() => {
        let index = this.contacts.findIndex((item: any) => item.id === userId);
        this.contacts.splice(index, 1);
        this.$updated.next({
          contacts: [...this.contacts],
        });
      });
  }
}
